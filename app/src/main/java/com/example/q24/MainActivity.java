package com.example.q24;

import android.os.Bundle;

import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;

import android.view.View;

import androidx.core.view.WindowCompat;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.example.q24.databinding.ActivityMainBinding;

import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    Button btn;
    EditText etext1, etext2;
    TextView text2;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btn = findViewById(R.id.btn);
        etext1 = findViewById(R.id.etext1);
        etext2 = findViewById(R.id.etext2);
        text2 = findViewById(R.id.text2);

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int num1 = Integer.parseInt(etext1.getText().toString());
                int num2 = Integer.parseInt(etext2.getText().toString());

                if (num1>num2)
                {
                    text2.setText(num1+" Is greater than "+num2);
                } else if (num2>num1)
                {
                    text2.setText(num2+" Is greater than "+num1);
                }
                else if (num1==num2)
                {
                    text2.setText(num1+" Is equals to "+num2);
                }

            }
        });


    }
}